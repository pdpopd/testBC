package org.ergoplatform

import akka.actor.{Actor, ActorLogging, ActorRef, Props, Terminated}

import scala.concurrent.ExecutionContext
import scala.util.{Failure, Success}
import scala.concurrent.duration._

object BlockChainNodeActor {
  def props(startBlockChain: BlockChain, knownPeers: Seq[ActorRef]): Props = Props(new BlockChainNodeActor(startBlockChain, knownPeers))

  case object GetBlockChain

  case object GetConnectedPeers

  case class ConnectTo(peer: ActorRef)

  case class Peers(kp: Seq[ActorRef])

  case object StopSync

  case object ScoreBroadcast

  case class Score(s: Long)

  case class SyncFrom(b: Option[Block])

  case class Contains(a: Option[Block], b: Option[Block])

  sealed trait Res

  case class Yes(a: Block) extends Res

  case class No(a: Block) extends Res

  case class ContainsRes(a: Res, b: Res)

}

class BlockChainNodeActor(startBlockChain: BlockChain, knownPeers: Seq[ActorRef]) extends Actor with ActorLogging {

  import BlockChainNodeActor._

  implicit val executorContext: ExecutionContext = context.dispatcher

  override def preStart(): Unit = {
    context.become(active(startBlockChain, knownPeers, None, None, 0))
    knownPeers.foreach(_ ! ConnectTo(self))
    context.system.scheduler.schedule(1.second, 1.second, () => self ! ScoreBroadcast)
  }

  private def sync(blockChain: BlockChain, peers: Seq[ActorRef], onlyFrom: Option[ActorRef], master: Option[(ActorRef, Long)]): Receive = {
    case m => log.debug(s"sync m: $m")
      m match {
        case ContainsRes(No(_), Yes(_)) =>
          context.become(sync(BlockChain(), peers, onlyFrom, master))
          sender() ! SyncFrom(None)
        case ContainsRes(No(_), No(_)) =>
          context.become(sync(BlockChain(), peers, onlyFrom, master))
          sender() ! SyncFrom(None)

        case ContainsRes(Yes(a), No(b)) =>
          val slice = blockChain.dropWhile(_.id != a.id).takeWhile(_.id != b.id)
          val s = slice.size
          if (s > 20) {
            val x = Contains(Some(a), slice.drop(s / 2).headOption)
            sender() ! x
          } else {
            val y = blockChain.takeWhile(_.id != a.id).map(a => (a.id, a)).toList
            val x = collection.immutable.ListMap.apply(y: _*)
            context.become(sync(BlockChain(x), peers, onlyFrom, master))
            sender() ! SyncFrom(Some(a))
          }

        case ContainsRes(Yes(a), Yes(b)) =>
          val slice = blockChain.dropWhile(_.id != b.id)
          val s = slice.size
          if (s > 20) {
            val x = Contains(Some(b), slice.drop(s / 2).headOption)
            sender() ! x
          }
          else {
            val y = blockChain.takeWhile(_.id != a.id).map(x => (x.id, x)).toList
            val x = collection.immutable.ListMap.apply(y: _*)
            context.become(sync(BlockChain(x), peers, onlyFrom, master))
            sender() ! SyncFrom(Some(a))
          }

        case StopSync =>
          context.become(active(blockChain, peers, None, None, blockChain.score))
        case b: Block if !blockChain.contains(b) && onlyFrom.getOrElse(sender()) == sender() =>
          blockChain.append(b) match {
            case Success(newBlockChain) =>
              context.become(sync(newBlockChain, peers, onlyFrom, master))
            case Failure(f) =>
              log.warning(s"sync Error on apply block $b to blockchain $blockChain: $f")
          }
        case other => log.debug(s"sync to trash $other"); self.tell(other, sender())
      }
  }

  private def active(blockChain: BlockChain, peers: Seq[ActorRef], onlyFrom: Option[ActorRef], master: Option[(ActorRef, Long)], scoreAfterSync: Long): Receive = {
    case m => log.debug(s"active m: $m")

      def startSync(): Unit = {
        context.become(sync(blockChain, peers, master.map(_._1), master))
        val x = Contains(blockChain.headOption, blockChain.lastBlock)
        if (blockChain.size < 20) {
          context.become(sync(BlockChain(), peers, master.map(_._1), master))
          sender() ! SyncFrom(None)
        } else x match {
          case Contains(None, None) =>
            context.become(sync(BlockChain(), peers, master.map(_._1), master))
            sender() ! SyncFrom(None)
          case _ => sender ! x
        }
      }

      m match {
        case GetConnectedPeers =>
          sender() ! peers
        case GetBlockChain =>
          sender() ! blockChain
        case Peers(s) =>
          s.filter(n => !peers.contains(n) && self != n).foreach { n =>
            context.watch(n)
            context.become(active(blockChain, peers :+ n, onlyFrom, master, scoreAfterSync))
            n ! Score(blockChain.score)
          }

        case ConnectTo(node) if !peers.contains(node) && node != self =>
          context.become(active(blockChain, peers :+ node, onlyFrom, master, scoreAfterSync))
          if (peers.nonEmpty)
            node ! Peers(peers)
          peers.foreach(_ ! Peers(peers :+ node))
          node ! Score(blockChain.score)

        case Terminated(terminatedNode) =>
          if (master.map(_._1).contains(terminatedNode))
            context.become(active(blockChain, peers.filter(_ != terminatedNode), onlyFrom, None, scoreAfterSync))
          else
            context.become(active(blockChain, peers.filter(_ != terminatedNode), onlyFrom, master, scoreAfterSync))

        case b: Block if !blockChain.contains(b) && Block.isValid(b, blockChain).isRight =>
          blockChain.append(b) match {
            case Success(newBlockChain) =>
              context.become(active(newBlockChain, peers, onlyFrom, master, scoreAfterSync))
            case Failure(f) =>
              log.warning(s"Error on apply block $b to blockchain $blockChain: $f")
          }
          if (master.isEmpty && onlyFrom.isEmpty)
            peers foreach (_ ! b)

        case Contains(None, None) =>
          blockChain.foreach(sender() ! _)
          sender() ! StopSync
        case Contains(Some(a), Some(b)) =>
          val x = blockChain.find(_ == a) match {
            case Some(_) => Yes(a)
            case None => No(a)
          }
          val y = blockChain.find(_ == b) match {
            case Some(_) => Yes(b)
            case None => No(b)
          }
          sender() ! ContainsRes(x, y)

        case SyncFrom(None) =>
          blockChain.foreach(sender() ! _)
          sender() ! StopSync
        case SyncFrom(Some(b)) =>
          (blockChain.dropWhile(_.id != b.id).toList :+ b).foreach(sender() ! _)
          sender() ! StopSync

        case Score(s) =>
          if (!peers.contains(sender()))
            context.become(active(blockChain, peers :+ sender(), onlyFrom, master, scoreAfterSync))
          if (s > blockChain.score && s > master.map(_._2).getOrElse(0L)) {
            context.become(active(blockChain, peers, Some(sender()), Some(sender() -> s), scoreAfterSync))
            startSync()
            peers.foreach(_.tell(Score(s), sender()))
          } else if (s < blockChain.score) {
            sender().tell(Score(blockChain.score), self)
          }

        case ScoreBroadcast =>
          if (master.map(_._2).getOrElse(0L) > blockChain.score)
            peers.foreach(_ ! Peers(peers :+ self))
          if (scoreAfterSync != blockChain.score)
            peers.foreach(p => p ! Score(blockChain.score))
        case other => log.debug(s"to trash $other score:${blockChain.score}")
      }
  }

  override def receive: Actor.Receive = Actor.emptyBehavior
}
